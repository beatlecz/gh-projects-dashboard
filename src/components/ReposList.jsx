import React from 'react'
import BaseComponent from '../lib/baseComponent'
import parseFilter from '../lib/parseFilter'
import Project from './Project'

class ReposList extends BaseComponent {
    constructor(props) {
        super(props)
        this._bind('getFilteredRepos')
    }

    getFilteredRepos() {
        let {filter, repos} = this.props
        let f = parseFilter(filter)

        return repos
            .filter(r => f.empty === "true" ? true : r.projects.length > 0)
            .filter(r => f.repository ? r.name.indexOf(f.repository) !== -1 : true)
    }

    render() {
        let {filterChange} = this.props

        return (
            <div>
                {this.getFilteredRepos().map((r,i) => (
                    <div key={i}>
                        <h4><a onClick={() => filterChange(`repository:${r.name}`)}>{r.name}</a></h4>
                        <hr />
                        {r.projects.map((p,i) => <Project key={i} project={p} />)}
                    </div>
                ))}
            </div>
        )
    }
}

export default ReposList