import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Login from './Login'
import * as Actions from '../actions'

function mapStateToProps(state) {
    return state.get('loginState').toJS()
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)