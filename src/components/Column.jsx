import React from 'react'
import BaseComponent from '../lib/baseComponent'
import {Panel, Badge} from 'react-bootstrap'
import Card from './Card'

const Header = ({column}) => (
    <strong>{column.name} <Badge>{column.cards.length}</Badge></strong>
) 

class Column extends BaseComponent {
    render() {
        let {column} = this.props

        return (
            <Panel 
                header={<Header column={column} />} 
                className="column"
            >
                {column.cards.map((c,i) => <Card key={i} card={c} />)}
            </Panel>
        )
    }
}

export default Column