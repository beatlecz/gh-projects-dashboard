import React from 'react'
import BaseComponent from '../lib/baseComponent'

import { Panel, Alert, Button, Form, FormGroup, FormControl, ControlLabel, Col } from 'react-bootstrap'

class Login extends BaseComponent {
    constructor(props) {
        super(props)
        this._bind('onFormSubmit')
    }

    onFormSubmit(e) {
        e.preventDefault()
        let {login} = this.props

        login(this.username.value, this.password.value)      
    }

    render() {
        let {message} = this.props

        return (
            <Col sm={4} smOffset={4}>
                <Panel header="Login">
                    <Form onSubmit={this.onFormSubmit}>                    
                        <FormGroup controlId="formHorizontalEmail">
                            <ControlLabel>Username</ControlLabel>
                            <FormControl inputRef={c => this.username = c} type="text" placeholder="Username" />
                        </FormGroup>

                        <FormGroup controlId="formHorizontalPassword">
                            <ControlLabel>Password</ControlLabel>
                            <FormControl inputRef={c => this.password = c} type="password" placeholder="Password" />
                        </FormGroup>

                        <FormGroup>
                            <Button type="submit" bsStyle="primary">
                                Login
                            </Button>
                        </FormGroup>
                    </Form>
                    {message ? <Alert bsStyle="danger">{message}</Alert> : null}
                </Panel>
            </Col>
        )
    }
}

export default Login