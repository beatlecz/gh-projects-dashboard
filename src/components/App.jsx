import React from 'react'
import BaseComponent from '../lib/baseComponent'
import {Nav, Navbar, NavItem} from 'react-bootstrap'
import Login from './LoginContainer'
import Dashboard from './DashboardContainer'
import Progress from './Progress'

class App extends BaseComponent {
    render() {
        let {loggedIn, logout, isLoading} = this.props
        
        return (
            <div>
                <Progress visible={isLoading} />
                <Navbar fixedTop={true}>
                    <Navbar.Header>
                        <Navbar.Brand>
                        <a>Github projects dashboard</a>
                        </Navbar.Brand>
                    </Navbar.Header>
                    <Nav pullRight>
                        {loggedIn 
                        ? <NavItem onClick={logout}>Logout</NavItem> 
                        : null}
                        
                    </Nav>
                </Navbar>
                <div className="container">
                    {loggedIn ? <Dashboard /> : <Login />}   
                </div>
            </div>
        )
    }
}

export default App
