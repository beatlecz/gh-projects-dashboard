import React from 'react'
import BaseComponent from '../lib/baseComponent'
import {FormControl, InputGroup, Button, Col, Row} from 'react-bootstrap'
import ReposList from './ReposList'

class Dashboard extends BaseComponent {    
    constructor(props) {
        super(props)
        this._bind('onFilterChange')
    }

    componentDidMount() {
        let {loadFullRepos} = this.props

        loadFullRepos()
    }

    onFilterChange() {
        let {filterChange} = this.props

        filterChange(this.filterInput.value)
    }

    render() {
        let {filter, repos, filterChange} = this.props

        return (
            <div>
                <h3>Projects</h3>
                <hr />
                <Row>
                    <Col sm={4}>
                        <InputGroup>
                            <FormControl
                                type="text"
                                value={filter}
                                placeholder="Filter repos and projects"
                                onChange={this.onFilterChange}
                                inputRef={c => this.filterInput = c}
                            />    
                            <InputGroup.Button>
                                <Button onClick={() => filterChange('')}>Clear</Button>
                            </InputGroup.Button>
                        </InputGroup>
                    </Col>
                </Row>
                <ReposList 
                    filter={filter} 
                    repos={repos} 
                    filterChange={filterChange} 
                />
            </div>
        )
    }
}

export default Dashboard