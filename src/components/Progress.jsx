import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

export default ({visible}) => {
    if (visible) NProgress.inc()
    else NProgress.done()
    
    return null
}