import React from 'react'
import BaseComponent from '../lib/baseComponent'
import {Panel} from 'react-bootstrap'
import Column from './Column'

class Project extends BaseComponent {
    render() {
        let {project} = this.props

        return (
            <Panel header={project.name} bsStyle="primary" className="project">
                {project.columns.map((c,i) => <Column key={i} column={c} />)}
            </Panel>
        )
    }
}

export default Project