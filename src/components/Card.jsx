import React from 'react'
import BaseComponent from '../lib/baseComponent'
import {Panel} from 'react-bootstrap'

class Card extends BaseComponent {
    render() {
        let {card} = this.props

        return (
            <Panel className="card">
                {card.note}
            </Panel>
        )
    }
}

export default Card