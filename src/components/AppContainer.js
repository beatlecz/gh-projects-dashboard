import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import App from './App'
import {logout} from '../actions'

function mapStateToProps(state) {
    return Object.assign({},
        state.get('loginState').toJS(),
        state.get('appState').toJS()
    )
}

function mapDispatchToProps(dispatch) {
    return {
        logout: bindActionCreators(logout, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)