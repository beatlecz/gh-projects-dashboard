import {createReducer} from 'redux-immutablejs'
import {fromJS} from 'immutable'

import {LOGIN, DASHBOARD} from './action-types'

const appState = createReducer(fromJS({isLoading: false}), {
    
    [LOGIN.REQUEST]: (state) => state.merge({
        isLoading: true
    }),

    [LOGIN.SUCCESS]: (state) => state.merge({
        isLoading: false
    }),

    [LOGIN.FAIL]: (state) => state.merge({
        isLoading: false
    }),

    [DASHBOARD.REPOS_REQUEST]: (state) => state.merge({
        isLoading: true
    }),

    [DASHBOARD.REPOS_RESPONSE]: (state) => state.merge({
        isLoading: false
    })

})

let initialLoginState = fromJS({
    loggedIn: false,
    userInfo: null,
    message: null
})
const loginState = createReducer(initialLoginState, {
    
    [LOGIN.SUCCESS]: (state, action) => state.merge({
        isLoading: false,
        userInfo: { 
            username: action.username
        },
        loggedIn: true
    }),

    [LOGIN.FAIL]: (state, action) => state.merge({
        message: action.message
    }),

    [LOGIN.LOGOUT]: (state, action) => state.merge({
        loggedIn: false
    })

})

let initialDashboardState = fromJS({
    isLoading: false,
    repos: [],
    filter: ''
})
const dashboardState = createReducer(initialDashboardState, {
    
    [DASHBOARD.REPOS_RESPONSE]: (state, action) => state.merge({
        repos: action.data
    }),

    [DASHBOARD.FILTER_CHANGE]: (state, action) => state.merge({
        filter: action.filter
    })

})

export {
    appState,
    loginState,
    dashboardState
}
