import action from './lib/baseAction'
import * as api from './api'
import {LOGIN, DASHBOARD} from './action-types'

// Login
//
export function login(username, password) {
    return dispatch => {
        dispatch(action(LOGIN.REQUEST))

        api.login(username, password)
            .then(() => dispatch(action(LOGIN.SUCCESS, {username})))
            .catch(message => dispatch(action(LOGIN.FAIL, {message})))
    }
}
export function logout() {
    api.logout()
    return action(LOGIN.LOGOUT)
}

// Dashboard
//

export function loadFullRepos() {
    return dispatch => {
        dispatch(action(DASHBOARD.REPOS_REQUEST))

        api.loadFullRepos()
            .then(data => {
                localStorage.data = JSON.stringify(data)
                dispatch(action(DASHBOARD.REPOS_RESPONSE, {data}))
            })
    }
}

export function filterChange(filter) {
    return action(DASHBOARD.FILTER_CHANGE, {filter})
}