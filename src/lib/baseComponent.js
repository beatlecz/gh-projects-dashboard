import {Component} from 'react'

export default class BaseComponent extends Component {
    _bind(...methods) {
        methods.forEach(m => this[m] = this[m].bind(this))
    }
}
