function parseFilter(filterText) {
    let filterObject = {}

    filterText.split(' ').forEach(f => {
        let filterItem = f.split(':')
        if (filterItem.length > 1) {
            filterObject[filterItem[0]] = filterItem[1]
        } else {
            filterObject['search'] += filterItem[0]
        }
    })

    return filterObject
}

export default parseFilter