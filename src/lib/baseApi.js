import axios from 'axios'

axios.defaults.headers.post['Content-Type'] = 'application/json'
axios.defaults.validateStatus = (status) => {
    return status < 400
}

export default axios
