export default function action(type, params) {
    // eslint-disable-next-line
    if (params && params.type) throw "You can't use 'type' keyword as property name."
    
    return Object.assign({type}, params)
}
