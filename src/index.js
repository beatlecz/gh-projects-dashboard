import React from 'react'
import {render} from 'react-dom'
import registerServiceWorker from './lib/registerServiceWorker';
import storeAppCreator from './lib/storeAppCreator'

import * as reducers from './reducers'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './style/main.css'

import App from './components/AppContainer'

// Setup component with injected reducers logic
let StoreApp = storeAppCreator(reducers, {mode: 'prod'})

render(
    <StoreApp>
        <App />
    </StoreApp>
    , document.getElementById('root')
)
registerServiceWorker()
