import at from './lib/actionType'

export const LOGIN = at('LOGIN',
    'REQUEST',
    'SUCCESS',
    'FAIL',
    'LOGOUT'
)

export const DASHBOARD = at('DASHBOARD',
    'REPOS_REQUEST',
    'REPOS_RESPONSE',
    'FILTER_CHANGE'
)

