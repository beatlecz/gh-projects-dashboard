import async from 'async-promises'
import api from './lib/baseApi'

let ghApi = api.create({
    baseURL: 'https://api.github.com/'
})

const PROJECTS_CONFIG = {
    headers: {
        'Accept': 'application/vnd.github.inertia-preview+json'
    }
} 

export function login(username, password) {
    return new Promise((resolve, reject) => {
        ghApi.get('/', {
            auth: {username, password}
        }).then(() => {
            ghApi.defaults.auth = {username, password}
            resolve()
        }).catch((error) => {
            reject(error.response.data.message)
        })
    })
}

export function logout() {
    return new Promise(resolve => {
        delete ghApi.defaults.auth
        resolve()
    })
}

function flattenArray(array) {
    return [].concat.apply([], array)
}

// Super cool parallel loading of entire repos/projects tree
//
export function loadFullRepos() {
    return new Promise((resolve, reject) => {
        loadRepos().then(repos => {
            async.parallel(repos.map(r => loadProjects(r))).then(projects => {
                projects.forEach((p,i) => repos[i].projects = p)
                let flatProjects = flattenArray(projects)

                async.parallel(flatProjects.map(p => loadColumns(p))).then(columns => {
                    columns.forEach((c,i) => flatProjects[i].columns = c)
                    let flatColumns = flattenArray(columns)

                    async.parallel(flatColumns.map(c => loadCards(c))).then(cards => {
                        cards.forEach((c,i) => flatColumns[i].cards = c)
                        resolve(repos)
                    })
                })
            })
        })
    })
    
}

export function loadRepos() {
    return new Promise((resolve, reject) => {
        ghApi.get('/user/repos')
            .then(response => resolve(response.data))
            .catch(reject)
    })
}

export function loadProjects(repo) {
    return new Promise((resolve, reject) => {
        ghApi.get(`/repos/${repo.owner.login}/${repo.name}/projects`, PROJECTS_CONFIG)
        .then(response => { resolve(response.data) })
        .catch(error => reject(error.response.data.message))
    })
}

export function loadColumns(project) {
    return new Promise((resolve, reject) => {
        ghApi.get(`/projects/${project.id}/columns`, PROJECTS_CONFIG)
        .then(response => { resolve(response.data) })
        .catch(error => reject(error.response.data.message))
    })
}

export function loadCards(column) {
    return new Promise((resolve, reject) => {
        ghApi.get(`/projects/columns/${column.id}/cards`, PROJECTS_CONFIG)
        .then(response => { resolve(response.data) })
        .catch(error => reject(error.response.data.message))
    })
}